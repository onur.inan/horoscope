//
//  HoroscopeCell.swift
//  Horoscope
//
//  Created by Onur İnan on 25.03.2021.
//

import UIKit

class HoroscopeCell: UICollectionViewCell {
    
    @IBOutlet weak var imgHoroscope: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    
}
