//
//  HoroscopeResponse.swift
//  Horoscope
//
//  Created by Onur İnan on 24.03.2021.
//

import Foundation
import ObjectMapper

class HoroscopeResponse : Decodable {
    var Data: [Horoscope]?
    var Success:Bool?
    var Error:String?
    var Message : String?
}


class Horoscope : Decodable{
    var ID : Int?
    var Name : String?
    var Properties:String?
    var DateInterval:String?
    var Icon:String?
    var LangID:Int?
    

}

