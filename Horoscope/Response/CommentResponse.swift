//
//  CommenResponse.swift
//  Horoscope
//
//  Created by Onur İnan on 2.04.2021.
//

import Foundation

class CommentResponse : Decodable{
    var Data: CommentData!
    var Success:Bool?
    var Error:String?
    var Message : String?

}

class CommentData: Decodable {
    var ID:Int?
    var HorosocopeID:Int?
    var Comment:String?
    var CreadteDate:String?
    
}
