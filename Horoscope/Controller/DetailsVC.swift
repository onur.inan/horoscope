//
//  DetailsVC.swift
//  Horoscope
//
//  Created by Onur İnan on 31.03.2021.
//

import UIKit
import Alamofire

class DetailsVC: UIViewController{
    @IBOutlet weak var segmentController: UISegmentedControl!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblProperties: UILabel!
    
    var horoscope: Horoscope!
    var features = ""
    var comment = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        let defaulttitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

        segmentController.setTitleTextAttributes(defaulttitleTextAttributes, for: .normal)
        segmentController.setTitleTextAttributes(titleTextAttributes, for: .selected)
        navigationItem.backButtonTitle=""
        navigationItem.title=horoscope.Name
        view.backgroundColor = UIColor(patternImage: UIImage(named:"background_horoscope")!)
        imgView.sd_setImage(with: URL(string: horoscope.Icon!), completed: nil)
        features = horoscope.Properties!.replacingOccurrences(of: "$", with: "\n\n")
        lblProperties.text=features
        
        self.navigationItem.leftBarButtonItem?.title=""
        
        getData()
    }
    
    
    @IBAction func didTapSegment(segment: UISegmentedControl){
        if segment.selectedSegmentIndex==0 {
            lblProperties.text = features
        } else {
            lblProperties.text = comment
        }
    }
    
    func getData(){
        let url = getHoroscope
        let params: [String : Int] = ["HoroscopeID":horoscope.ID!]
        
        
        AF.request("\(url)", method: .post, parameters:params).responseDecodable(of:CommentResponse.self)
        {
            response in
            if let data=response.value?.Data{
                self.comment=data.Comment!
            }
            
        }
        
    }
}



