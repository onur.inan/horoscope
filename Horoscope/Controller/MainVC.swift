//
//  MainVC.swift
//  Horoscope
//
//  Created by Onur İnan on 24.03.2021.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper
import SDWebImage
class MainVC:UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    var horoscope = [Horoscope]()
    @IBOutlet weak var collectionView: UICollectionView!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("başlatıldı")
        getData()
        collectionView.delegate=self
        collectionView.dataSource=self
        
        let bgImage = UIImageView();
            bgImage.image = UIImage(named: "background_horoscope");
            bgImage.contentMode = .scaleToFill
            self.collectionView?.backgroundView = bgImage
       
        print("Sonlandırıldı")
        
        // Do any additional setup after loading the view.
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return horoscope.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HoroscopeCell", for: indexPath) as!HoroscopeCell
        
        
        cell.lblName.text=horoscope[indexPath.row].Name
        cell.lblDate.text=horoscope[indexPath.row].DateInterval
        cell.imgHoroscope.sd_setImage(with: URL(string: horoscope[indexPath.row].Icon!), completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width
        return CGSize(width: width/3, height: 180)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let vc = segue.destination as? DetailsVC {
                vc.horoscope = sender as? Horoscope
            }
        }
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetail", sender: horoscope[indexPath.row])
    }
    
    func getData(){
        let url = horocopeURL
        let params: [String : Any] = ["LangID":"1"]
       
        
        AF.request("\(url)", method: .post, parameters:params).responseDecodable(of:HoroscopeResponse.self)
        {
            response in
            if let data=response.value?.Data{
                for data in data{
                    
                    self.horoscope.append(data)
                    self.collectionView.reloadData()
                }
            }
           
                     
        }
        
    }
  



}
